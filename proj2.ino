/*  Projeto 02 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
#include <Adafruit_GFX.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110
#include <Adafruit_PCD8544.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110

//defines
#define base 1000 //base de 1ms
#define SCLK 8 //
#define DIN 9 //
#define DC 10 // 
#define CSCE 11 //
#define RST 12 // 
#define ENBL 5  // 
#define INP1A 6 // 
#define INP2A 7 // 
#define func_vent "VENT" //
#define func_exaust "EXAUST" //
#define func_para "PARA" //
#define func_vel "VEL" //
#define func_ret "RETVEL" //
#define pinoInterruptExterna 3 //pino digital a ser definido para receber interrupção externa

//funções
void iniciaDisplay(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void interruptOn(); //ativa a interrução do timer 1
void interruptOff(); // desativa a interrupçaõ do timer 1
void iniciaPinosMotor();//faz a inicialização dos pinos do motor
void atualizaDisplay(bool isExaustor,bool movimento,int potencia,int velocidade); // atualização dos valores do display temporizada 
void defineSentido(bool horario); // define o sentido de giro do motor
void definePotencia(int potencia); //aplica a potencia dada na entrada PWM da ponte H
void freiaSistema(); // ativa configuração da ponte H para freiar mais rápido o motor
void serialRXFlush(); // Esvazia o buffer de recepção serial
void contaPulso(); //ISR externa


//variaveis e objetos globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
Adafruit_PCD8544 display = Adafruit_PCD8544(SCLK, DIN, DC, CSCE, RST); // Cria um objeto para tratar do display
bool isMoving = false; //variavel booleana para determinar se o sistema está se movendo
int power = 0; //intensidade da potencia em porcentagem
int RPM = 0; //Armazena a contagem de interrupções do fotransistor
bool isHorario = false; //identifica se o sentido é horário ou não
char entradaDado = '/0'; //valor de entrada de dados, iniciada em null
String stringEntrada = "";//String onde serão armazenadas os caracteres recebidos ao final do processo
String auxStringEntrada = ""; //String auxiliar onde serão armazenados os dados durante o recebimento
bool isNewInput = false; //verifica se há uma nova entrada de dados pelo serial
int timeOutCounter = 0;//temporizador de timeout para a recepção serial
bool isSerialError = false;//verifica se há erro na entrada serial
int lastRPM = 0;//armazena o RPM antes de tratar para evitar variações durante o processo
int estado = 0;//estados da FSM
int changed = 0;//verifica se alterou o estado de vent para exaust ou vice-versa
bool isVelError = false;//avisa se há erro na entrada do comando vel
int auxPower = 0; //variavel auxiliar para tratar a potencia recebida em vel

//programa
void setup() {
 iniciaDisplay();
 iniciaPinosMotor();
 interruptOn();
 Serial.begin(9600);//inicia a comunicação com o bluetooth
 attachInterrupt(digitalPinToInterrupt(pinoInterruptExterna),contaPulso,FALLING);
 }

void loop() {
  
  if (!isNewInput && Serial.available()>0){//verifica se há entrada no serial e ela se ela é nova
    isSerialError = false;//reinia a ocorrencia de erro no serial
    timeOutCounter = 0;//inicia o timer
    while(1){//loop de leitura
       if(Serial.available()>0 && timeOutCounter>1){//caso ainda haja algo no buffer, realiza a leitura este tempo foi incluso devido a limitações do hardware encontradas durante o desenvolvimento
        timeOutCounter = 0;//reinicia o temporizador de timeout
        entradaDado = Serial.read();//consome o byte (char) armazenado no buffer serial
        
        if (entradaDado == '*'){//caso encontra o *, esvazia o buffer, sinaliza nova entrada e segue para o tratamento da entrada
          serialRXFlush();
          isNewInput = true;
          break;
         }
         auxStringEntrada += entradaDado;//adiciona o caractere resolvido à String de entrada
        }
      
       if (timeOutCounter>=500){//caso de mais de 0,5s sem novos caracteres e sem encontrar o *
         isSerialError = true;//sinaliza erro de timeout
         isNewInput = false;//indica que não há nova entrada
         break;
         }
    }
     entradaDado = '/0';//reinicia a entrdada de dados
     stringEntrada = auxStringEntrada;//copia a string recebida
     auxStringEntrada = "";//reinicia a string para armazenar novos recebidos
   }
  

  if (timeCounter>=1000){//quando excede 1s, realiza a atualização dos dados disponiveis no LCD, atualiza os dados do motor funcionamento do motor e valor de RPM 
    lastRPM = RPM; // armazena o RPM atual
    atualizaDisplay(isHorario,isMoving,power,60*lastRPM/2);//coloca no display o RPM calculado
    RPM = 0; // reinicia a contagem

    if (changed == 0){//trata caso não haja troca de exaust para vent ou vice versa
      defineSentido(isHorario);//coloca no mesmo sentido a ponte H
      if(power>0){
        definePotencia(power);//aplica a potencia
        }else {
          analogWrite(ENBL,0);//trata caso power seja negativo
          }
    }
    
    if(changed == 1){//caso haja troca de sentido de rotação
      if(lastRPM != 0){//so autoriza a troca caso o motor esteja parado
      }else{//quando para, autoriza a troca de sentido
        changed = 0;//reinicia a variavel changed
        defineSentido(isHorario);//aplica o novo sentido
        if(power>0){//aplica a mesma potencia de antes
          definePotencia(power);
          }else {
            analogWrite(ENBL,0);//caso de power nulo ou negativo
            }
      }
    }
       timeCounter = 0;//reinicia o timer
      }



  if (isNewInput){//trata os casos quando há uma nova entrada serial

    if(stringEntrada == func_vent){//compara a entrada com a palavra do vent
      if(estado == 2 && lastRPM != 0){//verifica se estado atual é exaustor
        freiaSistema();
        changed = 1;  // sinaliza mudança     
        }
      isHorario = true;//horario foi definido como ventilador no nosso projeto
      isMoving = true;//indica que deve se mover   
      estado = 1;//coloca o estado como ventilador (1)
      Serial.println("OK VENT");//resposta serial indicando que o comando foi recebido e será executado dentro do proximo ciclo de atualização

      } else if(stringEntrada == func_exaust){//compara com a palavra do exaust
        if(estado == 1 && lastRPM != 0){//verifica o estado atual
          freiaSistema();
          changed = 1;
          }
        isHorario = false;//no nosso projeto, o exaustor roda em anti-horario
        isMoving = true;
        estado = 2;//exaustor é o estado 2
        Serial.println("OK EXAUST");//indica que o comando foi executado e será aplicado na proxima atualização

      }else if(stringEntrada == func_para){//identifica o comando de parada
        power = 0;//zera a potencia
        isMoving = false;//indica que não está movendo
        freiaSistema();
        estado = 0;// no projeto o estado parado é o estado zero
        Serial.println("OK PARA");//indica que foi recebido e aplicado na proxima atualização

      }else if (stringEntrada == func_ret){//identifica a função de retorno da velocidade
        Serial.println("VEL: "+String(lastRPM*60/2)+" RPM");//retorna o ultimo valor medido
      }

      else if(stringEntrada.substring(0,3) == func_vel){//identifica o começo de VEL
       
        if(stringEntrada.length() <= 3){//trata caso não contenha parametro, ou seja, a String só tem VEL
        Serial.println("ERRO: PARAMETRO AUSENTE");//Retorna a mensagem de erro
        isVelError = true;//indica que há erro
        } else if(stringEntrada.length()>7){//indica que caso seja maior que 7, há mais dados que deveriam haver, logo está incorreto
          Serial.println("ERRO: PARAMETRO INCORRETO");//mmensagem de erro
          }else{
            if(stringEntrada.substring(4,7) == "000"){//trata a string 000 diferentemente das outras, pois caso não sejam numeros depois de vel  o comando do Arduino retorna 0
              auxPower = 0;//zera o valor de power
              power = 0;//zera power
              isMoving = true;//apesar de zero, não está no estado parado
              
              Serial.println("OK VEL "+String(power)+"%");//comunica a implementação ocorrerá na proxima atualização
              } else if (stringEntrada.substring(4,7).toInt() > 0){//caso diferente de 000
                auxPower = (stringEntrada.substring(4,7).toInt()); // armazena em aux power
                if (auxPower>=0 && auxPower <= 100){//verifica se vai de 0 a 100%
                  power = auxPower;//atualiza a potencia
                  isMoving = true;//indica que esta movendo
                  Serial.println("OK VEL "+String(power)+"%");//indica que será implementado na proxima atualização
                }else {//caso seja maior que 100 ou menor que 0
                  isVelError = true;//indica erro
                  Serial.println("ERRO: PARAMETRO INCORRETO");//indica erro
                  }
            }else{//caso seja nulo é porque é um valor não puramente numérico, ou seja, é invalido e não deve atualizar
                  isVelError = true;
                  Serial.println("ERRO: PARAMETRO INCORRETO");//indica erro
            }
     }
   }

    if (stringEntrada.substring(0,3) != func_vel && stringEntrada != func_para && stringEntrada != func_exaust && stringEntrada != func_ret && stringEntrada != func_vent){
      Serial.println("ERRO:COMANDO INEXISTENTE");
      } //Indica que o comando recebido não é nenhum dos esperados, logo devemos desconsiderá-lo e indicar o erro
         
    
     stringEntrada = "";//reinicia a entrada
     isVelError = false;//os erros de velocidade
     isNewInput = false;//permite novas entradas
 }

 }

void contaPulso(){
  RPM = RPM+1;//apenas incrementa o RPM
  }

void serialRXFlush(){//esvazia o buffer serial do Arduino
    while(Serial.available()>0){
            Serial.read();
            }
  }

void freiaSistema(){//comandos para freiar o motor de acordo com o datasheet da ponte H
  digitalWrite(ENBL,HIGH);
  digitalWrite(INP1A,LOW);
  digitalWrite(INP2A,LOW);
 }


void definePotencia(int potencia){//define a nova potencia
  analogWrite(ENBL,map(potencia,1,100,55,255));//comando que mapeia a entrada dada no intervalo dado atraves de um teorema de tales
  }


void iniciaPinosMotor(){
    pinMode(ENBL, OUTPUT);
    pinMode(INP1A, OUTPUT);
    pinMode(INP2A, OUTPUT);
}

void defineSentido(bool horario){
    if(horario){//sentido da ponte H para ventilador
      digitalWrite(INP1A,HIGH);
      digitalWrite(INP2A,LOW);
    }
    if(!horario){//sentido da ponte H para Exaustor
      digitalWrite(INP1A,LOW);
      digitalWrite(INP2A,HIGH);
    }
}

void atualizaDisplay(bool isVent,bool movimento,int potencia,int velocidade){
  display.clearDisplay();//reseta os valores do display
  if (!movimento){//caso esteja parado
    display.println("Proj2 - EA076"); // linha 1
    display.println("ESTADO: PARADO"); // linha 2
    display.println("Potencia: 0"); // linha 3
    display.println("VEL: 0 RPM"); // linha 4  
   }

  if (movimento && isVent){//caso ventilador
    display.println("Proj2 - EA076"); // linha 1
    display.println("ESTADO: VENT"); // linha 2
    display.println("Potencia: " + String(potencia) + "%"); // linha 3
    display.println("VEL: " + String(velocidade) + " RPM"); // linha 4     
   }

  if (movimento && (!isVent)){//caso exaustor
    display.println("Proj2 - EA076"); // linha 1
    display.println("ESTADO: EXAUST"); // linha 2
    display.println("Potencia: " + String(potencia) + "%"); // linha 3
    display.println("VEL: " + String(velocidade) + " RPM"); // linha 4
   }
  display.display(); //apresenta os valores no display
 }

void iniciaDisplay(){//inicialização do display
  display.begin();
  display.setContrast(70 ); //Ajusta o contraste do display
  display.clearDisplay();   //Apaga o buffer e o display
  display.setTextSize(1);  //Seta o tamanho para caber na linha como o desejado
  display.setTextColor(BLACK); //Seta letra preta
  display.setCursor(0,0);  //Seta a posição do cursor
  display.println("Proj2 - EA076"); // linha 1
  display.println("ESTADO: "); // linha 2
  display.println("Potencia: "); // linha 3
  display.println("VEL: "); // linha 4
  display.display();// apresenta os dados na tela
 }
 
void increaseTime(){//interrupção temporizada a cada 0,001s
  timeCounter++;
  timeOutCounter++;
}

void interruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){//desliga as interrupcoes do timerOne
    Timer1.detachInterrupt(); 
  }
