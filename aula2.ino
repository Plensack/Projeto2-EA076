/*  Projeto 02 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

//defines
#define base 1000 //base de 1ms
#define SCLK 8
#define DIN 9
#define DC 10
#define CSCE 11 
#define RST 12
#define ENBL 5 
#define INP1A 6
#define INP2A 7

//funções
void iniciaDisplay(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void interruptOn(); //ativa a interrução do timer 1
void interruptOff(); // desativa a interrupçaõ do timer 1
void iniciaPinosMotor();
void atualizaDisplay(bool isExaustor,bool movimento,int potencia,int velocidade); // atualização dos valores do display temporizada 
void defineSentido(bool horario);

//variaveis e objetos globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
Adafruit_PCD8544 display = Adafruit_PCD8544(SCLK, DIN, DC, CSCE, RST);
bool isMoving = false;
int power = 0;
int RPM = 0;
bool isHorario = false;
int passo = 10;

//programa
void setup() {
 iniciaDisplay();
 iniciaPinosMotor();
 interruptOn();
 }

void loop() {
  if (timeCounter>=1000){
      analogWrite(ENBL,abs(power));
      timeCounter = 0;
      power = power+passo;
   }

  if (power >= 255){
      passo = -10;
  }

  if (power <= -255){
      passo = 10;
  }

  if (power > 0){
    isHorario = true;
    defineSentido(isHorario);
  }

  if (power < 0){
    isHorario = false;
    defineSentido(isHorario);
  }
}

void iniciaPinosMotor(){
    pinMode(ENBL, OUTPUT);
    pinMode(INP1A, OUTPUT);
    pinMode(INP2A, OUTPUT);
}

void defineSentido(bool horario){
    if(horario){
      digitalWrite(INP1A,HIGH);
      digitalWrite(INP2A,LOW);
    }
    if(!horario){
      digitalWrite(INP1A,LOW);
      digitalWrite(INP2A,HIGH);
    }
}

void atualizaDisplay(bool isVent,bool movimento,int potencia,int velocidade){
  display.clearDisplay();
  if (!movimento){
    display.println("Proj2 - EA076"); // linha 1
    display.println("ESTADO: PARADO"); // linha 2
    display.println("Potencia: 0"); // linha 3
    display.println("VEL: 0 RPM"); // linha 4  
   }

  if (movimento && isVent){
    display.println("Proj2 - EA076"); // linha 1
    display.println("ESTADO: VENT"); // linha 2
    display.println("Potencia: " + String(potencia) + "%"); // linha 3
    display.println("VEL: " + String(velocidade) + " RPM"); // linha 4     
   }

  if (movimento && (!isVent)){
    display.println("Proj2 - EA076"); // linha 1
    display.println("ESTADO: EXAUST"); // linha 2
    display.println("Potencia: " + String(potencia) + "%"); // linha 3
    display.println("VEL: " + String(velocidade) + " RPM"); // linha 4
   }
  display.display(); //apresenta os valores no display
 }

void iniciaDisplay(){
  display.begin();
  display.setContrast(60); //Ajusta o contraste do display
  display.clearDisplay();   //Apaga o buffer e o display
  display.setTextSize(1);  //Seta o tamanho para caber na linha como o desejado
  display.setTextColor(BLACK); //Seta letra preta
  display.setCursor(0,0);  //Seta a posição do cursor
  display.println("Proj2 - EA076"); // linha 1
  display.println("ESTADO: "); // linha 2
  display.println("Potencia: "); // linha 3
  display.println("VEL: "); // linha 4
  display.display();// apresenta os dados na tela
 }
 
void increaseTime(){
  timeCounter++;
}

void interruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){//desliga as interrupcoes do timerOne
    Timer1.detachInterrupt(); 
  }