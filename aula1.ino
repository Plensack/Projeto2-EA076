/* Projeto 02 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

//defines
#define base 1000 //base de 1ms
#define SCLK 8
#define DIN 9
#define DC 10
#define CSCE 11 
#define RST 12

//funções
void iniciaDisplay(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void interruptOn(); //ativa a interrução do timer 1
void interruptOff(); // desativa a interrupçaõ do timer 1
void atualizaDisplay(short int estado, int potencia, int velocidade); // atualização dos valores do display temporizada 

//variaveis e objetos globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
Adafruit_PCD8544 display = Adafruit_PCD8544(SCLK, DIN, DC, CSCE, RST);

//programa
void setup() {
 interruptOn();
 iniciaDisplay();
 }

void loop() {
    
 }

void iniciaDisplay(){
  //Serial.begin(9600); // ver se é necessário de fato
  display.begin();
  display.setContrast(60); //Ajusta o contraste do display
  display.clearDisplay();   //Apaga o buffer e o display
  display.setTextSize(1);  //Seta o tamanho para caber na linha como o desejado
  display.setTextColor(BLACK); //Seta letra preta
  display.setCursor(0,0);  //Seta a posição do cursor
  display.println("Proj2 - EA076"); // linha 1
  display.println("ESTADO: "); // linha 2
  display.println("Potencia: "); // linha 3
  display.println("VEL: "); // linha 4
  display.display();
 }
 
void increaseTime(){
    timeCounter++;
}

void interruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){//desliga as interrupcoes do timerOne
    Timer1.detachInterrupt(); 
  }
